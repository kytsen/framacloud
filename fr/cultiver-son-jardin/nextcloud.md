# Installation de Nextcloud

**NextCloud** est une solution complète permettant de faire avant tout
de l'hébergement de fichiers, en alternative à Dropbox.
Vous pouvez ainsi sauvegarder et synchroniser des fichiers depuis vos
différents appareils (ordinateur de bureau, ordiphone, tablette, ...).
Il est également possible d'installer des applications pour NextCloud,
étendant alors ses fonctionnalités.

NextCloud est le logiciel utilisé derrière **Framadrive**
(service d'hébergement de fichiers) et **Framagenda**
(service de gestion d'agendas, de contacts et de listes de tâches).

## Nourrir la terre

![](images/icons/preparer.png)

<div class="alert alert-info">
  <b>Deux installations sont possibles</b> : sur un serveur dédié, ou
  sur un hébergement mutualisé.
  Cependant, l’installation en mutualisé n’est pas celle que recommandent
  les développeurs et elle ne sera pas traitée dans ce tutoriel
  (votre serviteur n’a pas d’hébergement de test compatible).
  L'installation sur un hébergement mutualisé est toutefois possible à
  l'aide d'un unique fichier et est un peu documentée
  <a href="https://hagen.cocoate.com/2016/08/21/nextcloud-installation-shared-hosting-first-steps/">ici</a> (en anglais).
</div>

### Pré-requis

Nous allons partir du principe que vous avez accès à un serveur ayant
la configuration suivante ou équivalente :

*   La mémoire de la machine peut varier mais nous recommandons un minimum de 512MiB;
*   Linux Debian Jessie (Stable);
*   Serveur web Nginx installé;
*   PHP 5.6 ou PHP 7.0;
*   MySQL 5.5+ (PostgreSQL fonctionne également, SQLite est à déconseiller pour des raisons de performances).

Nous allons également assumer - mais ce n'est pas indispensable - que vous avez :

*   Un nom de domaine configuré pour rediriger vers votre serveur - ici nextcloud.exemple.org;
*   d’un serveur mail opérationnel pour confirmer les nouveaux comptes.

<div class="alert alert-info">
  Pour installer PHP sur Debian avec Nginx, n’installez pas le paquet
  php5 qui a pour dépendance Apache2, mais préférez le paquet php5-fpm
  (qui sera nécessaire pour la communication entre PHP et Nginx).
</div>

### Installation des modules de PHP

Pour fonctionner proprement, NextCloud a besoin d'un certain nombre de modules php.
Heureusement, la plupart sont fournis par défaut.
Voici comment installer ceux qui pourraient vous manquer :

    sudo apt install php5-curl php5-gd php5-tidy php5-mysql php5-intl\
     php5-mcrypt php5-imagick php-xml-parser`

Pour vérifier que tout ce qu’il faut est installé, un petit `php -m`
vous indiquera quels sont les modules utilisés par PHP
(en ligne de commande – ce n’est pas forcément ce qui sera utilisé par
le serveur web, mais c’est un moyen simple et rapide de voir si le module est installé).

Vous pouvez aussi vérifier que les modules sont activés pour php-fpm,
en vérifiant quels sont les modules activés dans `/etc/php5/mods-available/`.
Si vous voyez une ligne du genre `extension=tidy.so` sans point-virgule devant, c’est activé.

Vous devez retrouver ces éléments (il est possible que vous en ayez davantage)
dans la réponse de `php -m` : Cliquez sur « Détails » pour voir la liste des modules.

<details> bcmath bz2 calendar Core ctype curl date dba dom ereg exif
fileinfo filter ftp gd gettext hash iconv imagick intl json libxml
mbstring mcrypt mhash mysql mysqli openssl pcntl pcre PDO pdo_mysql
Phar posix readline Reflection session shmop SimpleXML soap sockets
SPL standard sysvmsg sysvsem sysvshm tidy tokenizer wddx xml xmlreader
xmlwriter Zend OPcache zip zlib </details>

Si vous avez ces modules installés, nous pouvons maintenant préparer la base de données.

### Configuration de MySQL

Nous allons utiliser la base de données **MySQL** pour enregistrer nos données.
Nous allons créer un utilisateur et une base de données pour NextCloud.
Si vous avez phpMyAdmin d'installé sur votre machine, vous pouvez également utiliser cet outil.

Connectons-nous au serveur MySQL :

    mysql -u root -p

et remplissez avec le mot de passe que vous avez choisi lors de
l'installation de MySQL lorsqu'on vous le demande.

Créons la base de données `nextcloud` :

    CREATE DATABASE nextcloud;

et créons un utilisateur `nextclouduser`

    CREATE USER "nextclouduser"@"localhost";

en lui définissant un mot de passe compliqué (typiquement, pas celui ci-dessous)

    SET password FOR "nextclouduser"@"localhost" = password('mot_de_passe_pour_nextcloud');

et enfin, en lui attribuant les droits sur notre nouvelle base de données :

    GRANT ALL ON nextcloud.* TO "nextclouduser"@"localhost";

Enfin, déconnectons-nous avec la commande `exit`.

## Semer

![](images/icons/semer.png)

### Téléchargement de NextCloud

Commencez par vous rendre sur cette page : <https://nextcloud.com/install/#instructions-server>
et notez le numéro de version de la dernière version stable.
Pour ma part, il s'agit de la version 10.0.1 à l'heure où ce tutoriel paraît.

Dans un terminal, entrez les commandes :

    cd /var/www/html
    sudo wget https://download.nextcloud.com/server/releases/nextcloud-10.0.1.zip
    sudo unzip nextcloud-10.0.1.zip`

Vous devrez changer la fin de ces deux dernières commandes avec le bon numéro de version.
Par exemple, ce serait `nextcloud-11.0.1` si la version était 11.0.1.

### Configuration de Nginx

Ensuite, créez un nouveau fichier de configuration pour nginx
`/etc/nginx/sites-available/nextcloud.conf`, par exemple avec nano :

    sudo nano /etc/nginx/sites-available/nextcloud.conf`

Copiez-collez-y le contenu suivant :

    server {
        listen 80 nextcloud.exemple.org;

        root /var/www/html;
        location /nextcloud/ {
            add_header Strict-Transport-Security "max-age=15768000; preload";

            gzip off;

            rewrite ^/caldav(.*)$ /remote.php/caldav$1 redirect;
            rewrite ^/carddav(.*)$ /remote.php/carddav$1 redirect;
            rewrite ^/webdav(.*)$ /remote.php/webdav$1 redirect;
            rewrite ^/apps/calendar/caldav.php /remote.php/caldav/ last;
            rewrite ^/apps/contacts/carddav.php /remote.php/carddav/ last;
            rewrite ^/remote/(.*) /remote.php last;

            rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
            rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json last;

            rewrite ^/.well-known/carddav /remote.php/carddav/ redirect;
            rewrite ^/.well-known/caldav /remote.php/caldav/ redirect;

            rewrite ^(/core/doc/[^\/]+/)$ $1/index.html;

            try_files $uri $uri/ /index.php$is_args$args;
            error_page 403 = /core/templates/403.php;
            error_page 404 = /core/templates/404.php;

            location ~ ^/(data|config|\.ht|db_structure\.xml|README) {
                deny all;
            }

            location ~ ^(.+?\.php)(/.*)?$ {
                try_files $1 =404;

                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$1;
                fastcgi_param PATH_INFO $2;
                fastcgi_param HTTPS $https;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_intercept_errors on;
                fastcgi_index index.php;
                fastcgi_buffers 64 4K;
            }

            location ~* ^.+\.(jpg|jpeg|gif|bmp|ico|png|css|js|swf)$ {
                expires 30d;
                access_log off;
            }
        }
    }


Activez ce nouveau fichier de configuration avec la commande

    sudo ln -s /etc/nginx/sites-available/nextcloud.conf /etc/nginx/sites-enabled/nextcloud.conf

Redémarrez le service nginx pour mettre en marche nos changements

    sudo service nginx restart

Ouvrez votre navigateur à l'adresse http://nextcloud.exemple.org.
Vous avez alors accès à un assistant de configuration.

## Arroser

![](images/icons/arroser.png)

L'assistant vous demande tout d'abord les informations de connexion de
votre compte utilisateur. Remplissez donc avec un identifiant et un mot
de passe solide.

Vous n'avez pas besoin de toucher au paramètre « Répertoire des données ».
Enfin, remplissez les informations de connexion à MySQL avec les valeurs suivantes :

Utilisateur de la base de données : `nextclouduser`

Mot de passe de la base de données : `mot_de_passe_pour_nextcloud` (celui que vous avez choisi au dessus)

Nom de la base de données : `nextcloud`

Ne pas toucher au nom du serveur (`localhost`).

En cliquant sur « Finaliser », vous êtes redirigé vers l'application.

## Regarder pousser

![](images/icons/pailler.png)

### Installer des applications

NextCloud est extensible en installant des applications que
l'administrateur peut installer en se rendant dans le menu principal en
haut à gauche, puis en cliquant sur « Applications ».
Nous recommandons les applications Contacts et Agenda.

### Autoriser les inscriptions

Pour autoriser les inscriptions sur votre instance de NextCloud,
vous devez installer l'application [Registration][1].

### Utiliser HTTPS

Pour utiliser https, il vous faudra générer des certificats TLS
(par exemple avec [Let's Encrypt][2]) et faire quelques ajustements en
s'inspirant de la [configuration proposée par NextCloud][3].

 [1]: https://github.com/pellaeon/registration
 [2]: https://letsencrypt.org/
 [3]: https://docs.nextcloud.com/server/10/admin_manual/installation/nginx_nextcloud_9x.html#nextcloud-in-a-subdir-of-nginx