# Framacloud – du libre dans les nuages

## 1 - Comprendre

### Quelques définitions

Le « cloud computing », traduction littérale « informatique dans les nuages »,
consiste en gros à « déplacer » les ressources informatique qu’on avait
auparavant sur nos ordinateurs (face à nous) vers des serveurs (sur internet).

Il se décompose en différents niveaux :

*   IaaS, Infrastructure as a Service : on partage le stockage et la puissance de calcul.
*   PaaS, Platform as a Service : a pour rôle l'exécution du logiciel.
    Elle est composée de briques utilisant des langages de programmation
    de haut niveau, généralement des langages de script (console de
    commande, Python, SQL, serveur d’application, etc.).
*   SaaS, Software as a Service : applicatifs en ligne.
    Gmail, Flickr, Facebook, Google Maps, etc.

Compte tenu de la place de plus en plus importante prise par le cloud
computing dans nos usages et notamment du fait qu’il soit porté par quelques
acteurs hégémoniques, dont les applications sont souvent gratuites mais
ont un code fermé et peu respectueux de la vie privée, l’association Framasoft
a proposé petit à petit différents services en ligne libres et accessibles à tous,
comme par exemple Framapad (éditeur collaboratif en ligne) ou
Framadate (outil de sondage et d’aide au choix de rendez-vous).

### Du Framalab à Dégooglisons Internet

En 2012, nous présentions ces différentes alternatives au cloud
et les problématiques soulevées.

<div class="col-xs-12 text-center">
    <video width="540" height="360" controls="controls" poster="https://framatube.org/images/media/904l.jpg">
        <source src="https://www.framatube.org/files/1289-framacloud-framalab.mp4" type="video/mp4" />
        <source src="https://www.framatube.org/files/1290-framacloud-framalab.webm" type="video/webm" />
    </video>
</div>


Avant d’entamer, en 2014, la campagne [Dégooglisons Internet](https://degooglisons-internet.org)
qui consistait à se lancer comme défi de proposer une trentaine de
services face aux services qui aspirent nos données et nous enferment.

<div class="col-xs-12 text-center">
    <video poster="https://framatube.org/blip/gosset-degooglisons-internet.png" controls="controls" width="540" height="360">
        <source src="https://framatube.org/blip/gosset-degooglisons-internet.webm" type="video/webm" />
        <source src="https://framatube.org/blip/gosset-degooglisons-internet.mp4" type="video/mp4"></video>
    <p class="text-center"><small>Conférence « Dégooglisons Internet » par Pyg au Capitole du Libre 2014.</small></p>
</div>

## 2 - Découvrir

Ainsi, lorsque nous utilisons internet, nous devons accorder notre
confiance à différents maillons de la chaîne qui nous relie aux données
personnelles placées en ligne.

À moins de s’auto-héberger, *le cloud est toujours l’ordinateur de quelqu’un d’autre*.
Il est donc important de savoir à qui nous faisons confiance et ce que
font les logiciels de nos données.

Pour choisir au mieux vos applications en ligne, afin de vous assurer
qu’elles respecteront vos données et votre vie privée, vous trouverez une
liste (non exhaustive) des alternatives classées selon le type d’usage sur

<p class="text-center">
    <a href="https://degooglisons-internet.org/alternatives" class="btn btn-warning btn-block">
        <i class="fa fa-fw fa-shield" aria-hidden="true"></i>
        Dégooglisons Internet
    </a>
</p>

Si vous cherchez un logiciel en particulier, vous pouvez effectuer une
recherche parmi les « Logiciels alternatifs » sur l’annuaire de Framasoft

<p class="text-center">
    <a href="https://framalibre.org/alternative-free-softwares" class="btn btn-info btn-block">
        <i class="fa fa-fw fa-linux" aria-hidden="true"></i>
        Framalibre
    </a>
</p>

Enfin, si vous chercher un hébergeur qui respecte vos données et propose
plusieurs services, nous vous invitons à vous tourner vers le collectif
CHATONS.

<p class="text-center">
    <a href="https://chatons.org" class="btn btn-soutenir btn-block">
        <i class="fa fa-fw fa-paw" aria-hidden="true"></i>
        Chatons
    </a>
</p>

## 3 - Reprendre le contrôle

S’héberger soi-même, c’est possible… ce n’est pas simple, mais ça
s’apprend.

Vous trouverez quelques pistes pour vous auto-héberger ici dans la rubrique

<p class="text-center">
    <a href="cultiver-son-jardin/index.html" class="btn btn-success btn-block">
        <i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></i>
        Cultiver son jardin
    </a>
</p>

ou dans le guide de Xavier Cartron

<p class="text-center">
    <a href="auto-hebergement/index.html" class="btn btn-primary btn-block">
        <i class="fa fa-fw fa-book" aria-hidden="true"></i>
        L’auto-hébergement facile
    </a>
</p>

Nous prévoyons d’ajouter également un guide sur [Yunohost](https://yunohost.org/),
un logiciel qui permet l’installation de services en ligne auto-hébergés
en quelques clics. Nous y contribuons actuellement pour packager les
différents services que nous proposons.

[![](images/framacloud_480.gif)](images/framacloud.gif)