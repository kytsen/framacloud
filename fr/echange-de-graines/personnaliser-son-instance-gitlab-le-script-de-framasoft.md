# Personnaliser son instance Gitlab : le script de Framasoft

Le script est sur <https://framagit.org/framasoft/framateam/tree/master/add-nav>

Installez-le avec

    git clone https://framagit.org/framasoft/framateam.git /opt/framateam


Il faut le lancer après l’installation et à chaque mise à jour de gitlab. Nous faisons cela avec

    apt-get install gitlab-ce && /opt/framateam/add-nav/add-nav.sh


Vous y trouverez comment changer les favicons, ajouter un bout de conf Nginx pour pouvoir utiliser Let's Encrypt pour Gitlab et Mattermost, proposer l’authentification standard par défaut si vous utilisez LDAP ou encore comment faire écouter le dæmon GitlabPages en IPv6.

Bien évidemment, il vous faudra enlever le bout de script qui ajoute la barre de navigation Framasoft hein 🙂